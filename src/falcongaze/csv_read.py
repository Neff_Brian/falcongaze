#!/usr/bin/env python3

import csv
import matplotlib.pyplot as plt
from matplotlib.legend_handler import HandlerLine2D
with open('../../vel_data.csv', newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter = ',')
	line_count = 0
	lin_x = []
	ang_z = []
	time = []
	cnt = 0
	for row in reader:
		#print(f'\t{row[0]} and {row[1]}')
		lin_x.append(float(row[0]))
		ang_z.append(float(row[1]))
		cnt+=1
		time.append(cnt/15)


#with open('../../conf_data.csv', newline='') as csvfile2:
#	reader = csv.reader(csvfile2, delimiter = ',')
#	line_count = 0
#	left_conf = []
#	right_conf = []
#	time2 = []
#	cnt = 0
#	for row in reader:
#		#print(f'\t{row[0]} and {row[1]}')
#		left_conf.append(float(row[0]))
#		right_conf.append(float(row[1]))
#		cnt+=1
#		time2.append(cnt/125)

plt.figure(1)
l_x, = plt.plot(time,lin_x,'b--')
l_x.set_label('lin_x input')
a_z, = plt.plot(time,ang_z,'r-')
a_z.set_label('ang_z input')
plt.xlabel('Time (seconds)')
plt.ylabel('Command Magnitude')
plt.legend()
#plt.show()

#plt.figure(2)
#plt.subplot(211)
#l_c, = plt.plot(time2,left_conf,'b-')
#l_c.set_label('Left eye confidence')
#plt.ylabel('Confidence')
#plt.title('Left Eye Confidence')
##plt.legend()
#plt.subplot(212)
#r_c, = plt.plot(time2,right_conf,'r-')
#r_c.set_label('Right eye confidence')
#plt.xlabel('Time (seconds)')
#plt.ylabel('Confidence')
#plt.title('Right eye confidence')
##plt.legend()
plt.show()

