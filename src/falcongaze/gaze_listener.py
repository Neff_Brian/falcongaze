#!/usr/bin/env python3
import zmq
import msgpack
#from msgpack import loads
import json
import rospy
from gazetracking.msg import PupilInfo, GazeInfo

context = zmq.Context()
# open a req port to talk to gaze
addr = '127.0.0.1'  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string('SUB_PORT')
sub_port = req.recv_string()

# open a sub port to listen to gaze
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))
sub.setsockopt_string(zmq.SUBSCRIBE, 'surface')
sub2 = context.socket(zmq.SUB)
sub2.connect("tcp://{}:{}".format(addr, sub_port))
sub2.setsockopt_string(zmq.SUBSCRIBE, 'pupil')

# Create publishers
pub_gaze = rospy.Publisher('/gaze_info', GazeInfo, queue_size=10)

def parse_gaze_pos(msg):
	''' Parse gaze_position into a GazeInfo message.

	Return GazeInfo message, or -1 if msg argument was empty.
	'''
	#msg may be a list of gaze positions
	#if len(msg) < 1: return -1

	#outmsg = GazeInfo()

	# Parse message data
	#outmsg.timestamp = msg['timestamp']
	#print(outmsg.timestamp)
	#outmsg.index = m['index']
	#outmsg.confidence = msg['confidence']
	#outmsg.norm_pos = msg['norm_pos']
	#outmsg.diameter = msg['diameter']
	#outmsg.method = msg['method']

	# Parse optional data
	#if 'ellipse' in msg:
	#	outmsg.ellipse_center = msg['ellipse']['center']
	#	outmsg.ellipse_axis = msg['ellipse']['axes']
	#	outmsg.ellipse_angle = msg['ellipse']['angle']

	# Warn that 3D data hasn't been parsed
	# TODO: parse 3D data
	#if 'method' == '3d c++':
	#	rospy.logwarn("3D information parser not yet implemented,\
	#		3D data from JSON message not included in ROS message.")

	#return outmsg


# Helper function that prints topic and message in a readable format
#def prettyprint(topic, msg):
#	string = "\n\n" + str(topic) + ":\n" + str(msg)
#	return string

if __name__ == "__main__":

	rospy.loginfo("Starting gaze listener.")
	print("Starting gaze listener")

	#rospy.init_node('gazelistener')

	print("listening for socket message....")
	while not rospy.is_shutdown():
		# Receive message from socket, convert it to Python dict
		topic = sub.recv_string()
		print(topic)
		msg = sub.recv()
		msg=msgpack.loads(msg,raw=False)
		#norm_pos = msg['norm_pos']
		print(msg)
		
		#print("Hello")
		#print(topic)
		#msg = sub2.recv()
		#msg=msgpack.loads(msg,raw=False)
		#print(msg)

		# Convert message to ROS message
		#rospy.logdebug("Reading gaze position: \n" + prettyprint(topic, msg))
			
		# Parse and publish
		#if topic == "gaze":
		#outmsg = parse_gaze_pos(msg)
		#if not outmsg == -1: pub_gaze.publish(outmsg)

		#else:
		#	rospy.logerr("Unrecognized topic from socket: " + topic)S
