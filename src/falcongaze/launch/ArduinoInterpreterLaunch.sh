#!/bin/bash
echo Starting Wheelchair Controller
source /opt/ros/noetic/setup.bash
source ~/gaze_ws/devel/setup.bash
rosrun falcongaze ArduinoInterpreter.py
echo Execution complete, shutting down now
