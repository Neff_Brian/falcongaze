#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64
import numpy as np
from scipy import signal

#The following code creates a publisher that will just publish a sin
#wave with two separate components to it (defined by freq1 and freq2).
def talker():
    pub = rospy.Publisher('sWave', Float64, queue_size = 100)
    rospy.init_node('talker', anonymous=True)
    
    #designed to mimic the data rate of the confidence publisher
    datarate = 150
    rate = rospy.Rate(datarate)
    cnt = 0
    freq1 = 2
    freq2 = 10
    
    #establish an empty list to ultimately create a window of data to filter
    data = []
    
    #establish the sampling frequency (fs) and cutoff frequency (fc)
    #of the filter
    fs = datarate
    #adjust this parameter up or down and observe the response
    fc = 5
    w=fc/(fs/2)
    #build the butterworth low pass filter
    b,a = signal.butter(5, w, 'low')
    
    while not rospy.is_shutdown():
        T=1/datarate
        x = cnt*T
        #create 100 sample window of data
        while(cnt<100):
            data.append(np.sin(freq1 * 2.0 * np.pi*x) + np.sin(freq2 * 2.0 * np.pi*x))
            cnt+=1
        
        #append another sample each time through the loop
        data.append(np.sin(freq1 * 2.0 * np.pi*x) + np.sin(freq2 * 2.0 * np.pi*x))
        cnt+=1
        
        #delete the first sample from the list to prevent it from
        #getting too long
        del data[0]
        
        #filter the 100 sample window of data
        data_low = signal.lfilter(b,a,data)
        
        #publish only the 100th sample to the topic
        pub.publish(data_low[99])
        
        rate.sleep()
        
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
