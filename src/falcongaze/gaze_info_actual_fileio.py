#!/usr/bin/env python3

# specify the name of the surface you want to use
surface_name = "Surface1"


import msgpack
from msgpack import loads
import subprocess as sp
from platform import system
import json
import rospy
from geometry_msgs.msg import Twist
from falcongaze.msg import gaze_msg, conf_msg
filename = "gaze_data.txt"
myfile = open(filename,'w')

try:
    from pymouse import PyMouse
except ImportError:
    msg = """
    Please install PyMouse from PyUserInput
    https://github.com/PyUserInput/PyUserInput

    pip install PyUserInput
    """
    print(msg)
    exit(1)


output = gaze_msg()

# Create publishers/subscribers
pub_gaze = rospy.Publisher('/gaze_info', gaze_msg, queue_size=10)


def vel_listener(data):
	output.lin_x = data.linear.x
	output.ang_z = data.angular.z


def conf_listener(data):
	output.conf_0 = data.conf_0
	output.conf_1 = data.conf_1

if __name__ == "__main__":

	rospy.loginfo("Starting Gaze Info process.")
	print("Gaze Info process running.")

	rospy.init_node('pupil_gaze')
	
	rospy.Subscriber('/conf_info', conf_msg, conf_listener)
	rospy.Subscriber('/cmd_vel', Twist, vel_listener)  
	
	cnt = 0
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

	while not rospy.is_shutdown():
		
		cnt=cnt+1	
		pub_gaze.publish(output)
		if(cnt % 20000 == 0):
			myfile.write(str(f'{output.conf_1:.5f}')+","+str(f'{output.conf_0:.5f}')+","+str(f'{output.lin_x:.5f}')+","+str(f'{output.ang_z:.5f}')+"\n")
		

