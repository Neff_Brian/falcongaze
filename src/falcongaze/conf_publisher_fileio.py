#!/usr/bin/env python3
import zmq
import msgpack
#from msgpack import loads
import json
import rospy
from falcongaze.msg import conf_msg

context = zmq.Context()
# open a req port to talk to pupil
addr = '127.0.0.1'  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string('SUB_PORT')
sub_port = req.recv_string()

# open a sub port to listen to pupil
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))
sub.setsockopt_string(zmq.SUBSCRIBE, 'pupil')


filename = "conf_data.txt"
myfile = open(filename,'w')


output = conf_msg()

# Create publishers
pub_conf = rospy.Publisher('/conf_info', conf_msg, queue_size=10)

def parse_pupil_conf(msg):
	''' Parse confidence from each pupil

	Return PupilInfo message, or -1 if msg argument was empty.
	'''
	#msg may be a list of pupil positions
	if len(msg) < 1: return -1

	outmsg = msg['confidence']

	return outmsg
	
if __name__ == "__main__":

	rospy.loginfo("Starting pupil listener.")
	print("Starting pupil listener")

	rospy.init_node('conf_info')
	right_conf = 0.0
	left_conf = 0.0
	counter = 1

	print("listening for socket message....")
	while not rospy.is_shutdown():
		# Receive message from socket, convert it to Python dict
		topic = sub.recv_string()
		msg = sub.recv()
		msg=msgpack.loads(msg,raw=False)
		#print(msg)
			
		# Parse and publish
		outmsg = parse_pupil_conf(msg)
		#if not outmsg == -1: pub_conf.publish(outmsg)
		if topic=="pupil.0.2d":
			right_conf = outmsg
		if topic=="pupil.1.2d":
			left_conf = outmsg
		print("Left Eye: " + str(f'{output.conf_1:.3f}') + "  Right Eye: " + str(f'{output.conf_0:.3f}'))
		#print("Left Eye: " + str(f'{left_conf:.3f}') + "  Right Eye: " + str(f'{right_conf:.3f}'))
		output.conf_0 = right_conf
		output.conf_1 = left_conf
		pub_conf.publish(output)
		if(counter % 4 == 0):
			myfile.write(str(f'{output.conf_1:.5f}')+"," + str(f'{output.conf_0:.5f}')+"\n")
			counter = 0
		counter+= 1
		
		
		
		
		
		
		
