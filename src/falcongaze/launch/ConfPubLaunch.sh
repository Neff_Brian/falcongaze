#!/bin/bash
echo Starting confidence publisher
source /opt/ros/noetic/setup.bash
source ~/gaze_ws/devel/setup.bash
rosrun falcongaze conf_publisher.py
echo Execution complete
