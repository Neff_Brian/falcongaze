#!/usr/bin/env python3

# specify the name of the surface you want to use
surface_name = "Surface1"


import msgpack
from msgpack import loads
import subprocess as sp
from platform import system
import json
import rospy
from geometry_msgs.msg import Twist
from falcongaze.msg import gaze_msg, conf_msg
from std_msgs.msg import Float64
from std_msgs.msg import Int32


try:
    from pymouse import PyMouse
except ImportError:
    msg = """
    Please install PyMouse from PyUserInput
    https://github.com/PyUserInput/PyUserInput

    pip install PyUserInput
    """
    print(msg)
    exit(1)


output = gaze_msg()

# Create publishers/subscribers
pub_gaze = rospy.Publisher('/gaze_info', gaze_msg, queue_size=1)

def surf_listener(data):
	#print(data.data)
	output.surface = data.data

def vel_listener(data):
	output.lin_x = data.linear.x
	output.ang_z = data.angular.z


def conf_listener(data):
	output.conf_0 = data.conf_0
	output.conf_1 = data.conf_1

if __name__ == "__main__":

	rospy.loginfo("Starting Gaze Info process.")
	print("Gaze Info process running.")

	rospy.init_node('pupil_gaze')
	rate = rospy.Rate(125)
	
	rospy.Subscriber('/conf_info', conf_msg, conf_listener)
	rospy.Subscriber('/surf_pos', Int32, surf_listener)
	rospy.Subscriber('/gaze_cmd_vel', Twist, vel_listener)                                                                                                                                                                                                                                                                                                                                                                                                                                                                            

	while not rospy.is_shutdown():
		
		
		pub_gaze.publish(output)
		rate.sleep()


