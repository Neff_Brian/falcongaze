#!/usr/bin/env python3

import time

# https://github.com/pupil-labs/pyndsi/tree/v1.0
import ndsi  # Main requirement

GAZE_TYPE = "imu"  # Type of sensors that we are interested in
SENSORS = {}  # Will store connected sensors

import msgpack
from msgpack import loads
import subprocess as sp
from platform import system
import json
import rospy
from falcongaze.msg import gaze_msg

output = gaze_msg()
pub_inv = rospy.Publisher('/gaze_info', gaze_msg, queue_size = 10)


def main():
    # Start auto-discovery of Pupil Invisible Companion devices
    network = ndsi.Network(formats={ndsi.DataFormat.V4}, callbacks=(on_network_event,))
    network.start()
    
    rospy.init_node('pupil_inv')

    try:
        # Event loop, runs until interrupted
        while network.running:
            # Check for recently connected/disconnected devices
            if network.has_events:
                network.handle_event()

            # Iterate over all connected devices
            for imu_sensor in SENSORS.values():
                # Fetch recent sensor configuration changes,
                # required for pyndsi internals
                while imu_sensor.has_notifications:
                    imu_sensor.handle_notification()

                # Fetch recent gaze data
                for imu in imu_sensor.fetch_data():
                    # Output: GazeValue(x, y, ts)
                    print(imu_sensor, imu)
                    #print("X Position: " + str(gaze.x) + "Y Position: " + str(gaze.y))
                    #output.lin_x = gaze.x
                    #output.ang_z = gaze.y
                    #pub_inv.publish(output)
                    #print("gyro x is " + str(imu.gyro.x))
                    #print("gyro y is " + str(imu.gyro.y))


            time.sleep(0.1)

    # Catch interruption and disconnect gracefully
    except (KeyboardInterrupt, SystemExit):
        network.stop()


def on_network_event(network, event):
    # Handle gaze sensor attachment
    if event["subject"] == "attach" and event["sensor_type"] == GAZE_TYPE:
        # Create new sensor, start data streaming,
        # and request current configuration
        sensor = network.sensor(event["sensor_uuid"])
        sensor.set_control_value("streaming", True)
        sensor.refresh_controls()

        # Save sensor s.t. we can fetch data from it in main()
        SENSORS[event["sensor_uuid"]] = sensor
        print(f"Added sensor {sensor}...")

    # Handle gaze sensor detachment
    if event["subject"] == "detach" and event["sensor_uuid"] in SENSORS:
        # Known sensor has disconnected, remove from list
        SENSORS[event["sensor_uuid"]].unlink()
        del SENSORS[event["sensor_uuid"]]
        print(f"Removed sensor {event['sensor_uuid']}...")


main()  # Execute example
