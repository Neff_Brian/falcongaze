#!/usr/bin/env python3

# specify the name of the surface you want to use
surface_name = "Surface1"

import zmq
import msgpack
from msgpack import loads
import subprocess as sp
from platform import system
import json
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from scipy import signal
import numpy as np

dataAng = [0.0, 0.0]
dataLin = [0.0, 0.0]

try:
    from pymouse import PyMouse
except ImportError:
    msg = """
    Please install PyMouse from PyUserInput
    https://github.com/PyUserInput/PyUserInput

    pip install PyUserInput
    """
    #print(msg)
    exit(1)


m = PyMouse()

context = zmq.Context()
# open a req port to talk to gaze
addr = '127.0.0.1'  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string('SUB_PORT')
sub_port = req.recv_string()

# open a sub port to listen to gaze
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))
sub.setsockopt_string(zmq.SUBSCRIBE, u'')

output = Twist()

# Create publishers/subscribers
pub_vel = rospy.Publisher('/gaze_cmd_vel', Twist, queue_size=10)
pub_plotter = rospy.Publisher('sWave', Float64, queue_size = 1)


def parse_surface_pos(msg):
	''' Parse gaze_position into a Twist message.

	Return Twist message, or -1 if msg argument was empty.
	'''
	x_dim, y_dim = m.screen_size()
	x = 0
	y = 0
	
	#msg may be a list of gaze positions
	if len(msg) < 1: 
		return -1

	#code taken from mouse_control.py and adapted:
	#getting position of gaze on surface1
	topic, msg = sub.recv_multipart()
	#try:
	#	gaze_position = loads(msg, encoding="utf-8")
		
	#except:
	#	print("invalid message")
	
	#try:
	#	if gaze_position["name"] == surface_name:
	#		gaze_on_screen = gaze_position["gaze_on_surfaces"]
	#		if len(gaze_on_screen) > 0:
	#			# or just use the most recent gaze position on the surface
	#			raw_x, raw_y = gaze_on_screen[-1]["norm_pos"]
#
#				x = raw_x
#				y = raw_y
#				
#
#	except:
#		print("not on screen")


	

# Helper function that prints topic and message in a readable format
def prettyprint(topic, msg):
	string = "\n\n" + str(topic) + ":\n" + str(msg)
	return string


		


if __name__ == "__main__":

	rospy.loginfo("Starting Gaze Info process.")
	print("Starting Gaze Info process.")

	rospy.init_node('vel_info')
	rate = rospy.Rate(30)
	
	lin_x = 0.0
	ang_y = 0.0

	print("listening for socket message....")
	while not rospy.is_shutdown():
		# Receive message from socket, convert it to Python dict
		topic = sub.recv_string()
		#print(topic)
		#msg = sub.recv()
		#msg=msgpack.loads(msg,raw=False)
		#print(msg)
		

		# Convert message to ROS message
		#rospy.logdebug("Reading surface position: \n" + prettyprint(topic, msg))
		#print(msg)
			
		# Parse and publish
		#if topic == "gaze":
		#parse_surface_pos(msg)

		
		
		#output.conf_0 = mode_determination(msg)
		#if not output == -1: 
		#pub_vel.publish(output)
		#pub_plotter.publish(output.angular.z)
		
		rate.sleep()

		#else:
		#	rospy.logerr("Unrecognized topic from socket: " + topic)


