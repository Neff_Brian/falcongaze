#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float64
import numpy as np
from scipy import signal
from falcongaze.msg import conf_msg

#establish an empty list to ultimately create a window of data to filter
data = []


#The following code creates a publisher that will just publish a sin
#wave with two separate components to it (defined by freq1 and freq2).
def talker(conf_data):
	pub = rospy.Publisher('sWave', Float64, queue_size = 1)
	
    
	#designed to mimic the data rate of the confidence publisher
	datarate = 150
	rate = rospy.Rate(datarate)

    
	#establish the sampling frequency (fs) and cutoff frequency (fc)
	#of the filter
	fs = datarate
	#adjust this parameter up or down and observe the response
	fc = 2
	w=fc/(fs/2)
	#build the butterworth low pass filter
	b,a = signal.butter(5, w, 'low')
    
	data.append(conf_data.conf_1)
    
	if(len(data)>100):
        	
		#delete the first sample from the list to prevent it from
		#getting too long
		del data[0]
        
		#filter the 100 sample window of data
		data_low = signal.lfilter(b,a,data)
        
		#publish only the 100th sample to the topic
		pub.publish(data_low[99])
        
	rate.sleep()

def listener():
	rospy.Subscriber('/conf_info', conf_msg, talker)
	
	rospy.spin()

if __name__ == '__main__':
	rospy.init_node('talker', anonymous=True)
	try:
		listener()
        
	except rospy.ROSInterruptException:
		pass
