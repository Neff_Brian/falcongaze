#!/usr/bin/env python3
import numpy as np
from matplotlib import pyplot as plt
import rospy
from std_msgs.msg import Float64

xs = []
ys = []

def plot_x(msg):
    global counter
    global xs
    global ys
    #if counter % 10 == 0:
        #stamp = msg.header.stamp
    print(msg.data)
    plt.ylim((-1.5,1.5))
    xs.append(counter)
#     ys.append(msg.data)
    xs = xs[-100:]
#     ys = ys[-100:]
#     if(counter > 100):
    plt.xlim((xs[0],xs[99]))
    plt.plot(counter,msg.data,'.')
#     plt.axis("equal")
    plt.draw()
    plt.pause(.000001)

    counter += 1

if __name__ == '__main__':
    counter = 0

    rospy.init_node("plotter")
    rospy.Subscriber("sWave", Float64, plot_x)
    plt.ion()
    plt.show()
    rospy.spin()