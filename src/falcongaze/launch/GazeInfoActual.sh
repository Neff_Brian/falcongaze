#!/bin/bash
echo Starting gaze info publisher
source /opt/ros/noetic/setup.bash
source ~/gaze_ws/devel/setup.bash
rosrun falcongaze gaze_info_actual.py
echo Execution complete
