#!/usr/bin/env python3
import zmq
import msgpack
#from msgpack import loads
import json
import rospy
from falcongaze.msg import conf_msg
from std_msgs.msg import Float64
from scipy import signal
import numpy as np

context = zmq.Context()
# open a req port to talk to pupil
addr = '127.0.0.1'  # remote ip or localhost
req_port = "50020"  # same as in the pupil remote gui
req = context.socket(zmq.REQ)
req.connect("tcp://{}:{}".format(addr, req_port))
# ask for the sub port
req.send_string('SUB_PORT')
sub_port = req.recv_string()

# open a sub port to listen to pupil
sub = context.socket(zmq.SUB)
sub.connect("tcp://{}:{}".format(addr, sub_port))
sub.setsockopt_string(zmq.SUBSCRIBE, 'pupil')


output = conf_msg()
dataRight = []
dataLeft = []

# Create publishers
pub_conf = rospy.Publisher('/conf_info', conf_msg, queue_size=1)
pub_plotter = rospy.Publisher('sWave', Float64, queue_size = 1)



def parse_pupil_conf(msg):
	''' Parse confidence from each pupil

	Return PupilInfo message, or -1 if msg argument was empty.
	'''
	#msg may be a list of pupil positions
	if len(msg) < 1: return -1

	outmsg = msg['confidence']

	return outmsg
	
	
def filter(conf_data, array):
	# Build filter
	datarate = 150 #should oversample compared to rospy.rate? we think?
	#establish the sampling (fs) and cutoff (fc) frequency of the filter
	fs = datarate
	fc = 2
	w=fc/(fs/2)
	#build the butterworth low pass filter
	b,a = signal.butter(5, w, 'low')
	array.append(conf_data)
    
	if(len(array)>100):
        	
		#delete the first sample from the list to prevent it from getting too long
		del array[0]
        
		#filter the 100 sample window of data
		data_low = signal.lfilter(b,a,array)
        	
		rate.sleep()
        	
		#return only the 100th sample
		return data_low[99]

        
	
if __name__ == "__main__":

	rospy.loginfo("Starting pupil listener.")
	print("Starting pupil listener")

	rospy.init_node('conf_info')
	rate = rospy.Rate(300)
	
	right_conf = 0.0
	left_conf = 0.0
	counter = 1
	print("listening for socket message....")
	while not rospy.is_shutdown():
		# Receive message from socket, convert it to Python dict
		topic = sub.recv_string()
		msg = sub.recv()
		msg=msgpack.loads(msg,raw=False)
			
		# Parse and publish
		outmsg = parse_pupil_conf(msg)
		#if not outmsg == -1: pub_conf.publish(outmsg)
		if topic=="pupil.0.2d":
			right_conf = outmsg
		if topic=="pupil.1.2d":
			left_conf = outmsg
		#print("Left Eye: " + str(f'{output.conf_1:.3f}') + "  Right Eye: " + str(f'{output.conf_0:.3f}'))
		#print("Left Eye: " + str(f'{left_conf:.3f}') + "  Right Eye: " + str(f'{right_conf:.3f}'))
		
		
		if(counter % 4 == 0):
			output.conf_0 = filter(right_conf, dataRight)
			output.conf_1 = filter(left_conf, dataLeft)
			pub_conf.publish(output)
			pub_plotter.publish(output.conf_1)
			
			counter = 0
		counter+= 1
		
		#ate.sleep()
		
