#!/usr/bin/env python3


import matplotlib.pyplot as plt
import rospy
import tf
from std_msgs.msg import Float64
import numpy as np
from matplotlib.animation import FuncAnimation
from scipy import signal

frequency = 12
x_axis_window_size = 10
ymax = 15
ymin = -15

x_updater = frequency * x_axis_window_size


class Visualiser:
    def __init__(self):
        self.fig, self.ax = plt.subplots()
        self.ln, = plt.plot([], [], '-r')
        plt.xlabel('Time (seconds)')
        plt.ylabel('Magnitude')
        self.x_data, self.y_data = [] , []
        self.y_low = []

    def plot_init(self):
        self.ax.set_xlim(0, x_axis_window_size)
        self.ax.set_ylim(ymin, ymax)
        return self.ln

    def odom_callback(self, msg):
        self.y_data.append(msg.data)
        x_index = len(self.x_data)
        time = x_index/frequency
        self.x_data.append(time)

    def update_plot(self, frame):
        self.ln.set_data(self.x_data, self.y_data)
        if(len(self.x_data)>x_updater):
            lower = len(self.x_data) - x_updater
            upper = len(self.x_data) - 1
            self.ax.set_xlim(self.x_data[lower],self.x_data[upper])
        return self.ln

rospy.init_node("plotter")
vis = Visualiser()
sub = rospy.Subscriber('sWave', Float64, vis.odom_callback)
ani = FuncAnimation(vis.fig, vis.update_plot, init_func=vis.plot_init, interval = 100)
plt.show(block=True)
